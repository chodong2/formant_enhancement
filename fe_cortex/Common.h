#include <stdio.h>

#define ALPHA 0.8f
#define FRONTMARGIN 48
#define PI2  6.283185307f
#define PI1  (PI2 * 0.5f)



#define NO_ERROR                  0


#define M                         10
#define FFTSIZE                   128
#define FFTSIZE_BY_TWO		      (FFTSIZE>>1)
#define FRAMESIZE                 80
#define Fs                        8000

void r_fft(float *farray_ptr, int isign);
void preemphasis(int len,float *buf,float *prev);
void deemphasis(int len,volatile float *buf,float *prev);