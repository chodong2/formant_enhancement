#ifndef _FE_INIT_H_
#define _FE_INIT_H_

#define FRAMESIZE 80

extern void formant_enhancement( void );

extern volatile float Out_Buf[FRAMESIZE];
extern short spk_in[FRAMESIZE];

#endif