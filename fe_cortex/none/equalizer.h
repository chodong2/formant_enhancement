#ifndef EQUALIZER_H

#define EQUALIZER_H

#define N_BAND8     8
#define N_BAND16    16

int equalizer(float *FFT_Buf, int *eqCurve);

#endif