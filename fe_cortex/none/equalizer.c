#include "Common.h"
#include "equalizer.h"
#include "rom_s-peaker.h"

int equalizer(float *FFT_Buf, int *eqCurve)
{
    int i, j;
    int ed, st;
    float gain;

    ed = Eq8Band[0];
    gain = EqCurveTable[eqCurve[0]];

    for (j = 0; j < ed; j++)
    {
        FFT_Buf[j << 1] *= gain;
    }
    st = ed;

    for (i = 1; i < N_BAND8; i++)
    {
        ed = Eq8Band[i];
        gain = EqCurveTable[eqCurve[i]];
        for (j=st; j < ed; j++)
        {
            FFT_Buf[j << 1] *= gain;
            FFT_Buf[(j << 1) + 1] *= gain;
        }
        st = ed;
    }
    FFT_Buf[1] *= gain;
    return NO_ERROR;
}
