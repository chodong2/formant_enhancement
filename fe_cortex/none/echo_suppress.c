#include "echo_suppress.h"
#include "Common.h"
#include "policy.h"
#include "rom_s-peaker.h"


float preemph_last_AES = 0.f;
float deemph_last_AES = 0.f;
float preemph_last1 = 0.f;

float in_buf_far[FFTSIZE], in_buf_near[FFTSIZE];
float previn_buf_far[FFTSIZE], previn_buf_near[FFTSIZE];
float synth_esti_far[FFTSIZE], synth_buf_near[FFTSIZE];
float prevsynth_esti_far[FFTSIZE], prevsynth_buf_near[FFTSIZE];
float out_buf_far[FRAMESIZE], out_buf_near[FRAMESIZE];

void noise_suprs(float *farray_ptr);

short echo_signal[FRAMESIZE];

// AES Function
void EC(short x[], short y[], short out_buf[]);
void preemphasis1(short len1, float *buf1, float *prev1);
void init_window1(float *w, short m, short l, short d);
void apply_window(float *buf, const float *w, short size);
void modified_bessel(float w, float *a0, float *a1);
float compute_gain(float pri, float post);
float SAFE_EXP(float x);
float SAFE_SAP(float x);

// AES Variable
//float Window[FFTSIZE];
float fftbuf_far[FFTSIZE], fftbuf_near[FFTSIZE];
float ifft_buf_far[FFTSIZE], ifft_buf_near[FFTSIZE], esti_far[FFTSIZE];
float farend[FFTSIZE];
float nearend[FFTSIZE];
float conju_far[FFTSIZE];
float far_near[FFTSIZE];
float far_far[FFTSIZE];
float far_power[NUM_CHAN], far_mag[NUM_CHAN];
float far_near_power[NUM_CHAN], far_near_mag[NUM_CHAN], far_far_mag[NUM_CHAN];
float near_power[NUM_CHAN], near_mag[NUM_CHAN];

float far_gain[NUM_CHAN], temp_far_gain[NUM_CHAN], far_far_power[NUM_CHAN];
float estimated_echo[NUM_CHAN];

float tmp_buf_near[FRAMESIZE], tmp_buf_far[FRAMESIZE];

float para_Gv;

float enhance_power[NUM_CHAN], se_enhance_power[NUM_CHAN];

float input[FFTSIZE];
float error[FFTSIZE];
float echo[FFTSIZE];
float ifft_buf[FFTSIZE];

static float a12[NUM_CHAN];
static float a22[NUM_CHAN];
static float long_far[NUM_CHAN], long_near[NUM_CHAN];
static float long_far_near[NUM_CHAN], long_far_far[NUM_CHAN];
static float p_d[FRAMESIZE], p_zd[FRAMESIZE], p_e[FRAMESIZE], p_ze[FRAMESIZE], p_z[FRAMESIZE];
static float rho_zd[FRAMESIZE], rho_ze[FRAMESIZE];

short dtd_flag;
short farend_flag;

float farend_sum;

float cross_rho_zd[NUM_CHAN], cross_p_d[NUM_CHAN], cross_p_z[NUM_CHAN], cross_p_zd[NUM_CHAN], cross_p_e[NUM_CHAN], cross_p_ze[NUM_CHAN];
float cross_rho_ze[NUM_CHAN];
float dtd_para1;
float a_opt[NUM_CHAN];
float dd;
float a_p;
float m[NUM_CHAN];
float sum_cross_zd, sum_cross_ze;

float snr_pri[NUM_CHAN];
float dd_snr[NUM_CHAN];

float temp_tmp_buf_near[FFTSIZE];
extern float evrc_noise[NUM_CHAN];

float temp_estimated_echo[NUM_CHAN];

float wie_gain[NUM_CHAN];
float gammaf[NUM_CHAN];

static float F_es[NUM_CHAN];
static float prev_F_es[NUM_CHAN];

float pri_snr[NUM_CHAN];
float post_snr[NUM_CHAN];
float ftmp;

float llrt[NUM_CHAN], lrt[NUM_CHAN];
float lsap[NUM_CHAN];

float agc_gain[NUM_CHAN];
float agc_gain_temp[NUM_CHAN];

extern short FrameNo;

//------------------ The echo cancellation function -----------------------
void Echo_suppress(short x[], short y[], short out_buf[])
{
    int i, k;
    short j, j1, j2;

    farend_sum = 0.0;
    for (i = 0; i < FRAMESIZE; i++)
    {

        tmp_buf_far[i] = (float)x[i]; //short형 echo frame x[] float형으로 변환
        tmp_buf_near[i] = (float)y[i];
        temp_tmp_buf_near[i] = (float)y[i];

        farend_sum += SQ(tmp_buf_far[i]);
    }

    for (i = 0; i < FRAMESIZE; i++)
    {
        input[i] = tmp_buf_near[i];
        echo[i] = tmp_buf_far[i];
    }

    for (i = FRAMESIZE; i < FFTSIZE; i++)
        temp_tmp_buf_near[i] = 0.0;
    /******************* noise suppression*********************/

    noise_suprs(temp_tmp_buf_near);

    /**********************************************************/

    init_window1(Window, FFTSIZE, FRAMESIZE, FRONTMARGIN);

    // Preemphasis 
    preemphasis(FRAMESIZE, tmp_buf_far, &preemph_last_AES);
    preemphasis1(FRAMESIZE, tmp_buf_near, &preemph_last1);

    // Make FFT Buffer 
    for (k = 0; k < FRONTMARGIN; k++)
    {
        in_buf_far[k] = previn_buf_far[FRAMESIZE + k];
        in_buf_near[k] = previn_buf_near[FRAMESIZE + k];
    }
    for (k = FRONTMARGIN; k < (FRONTMARGIN + FRAMESIZE); k++)
    {
        in_buf_far[k] = tmp_buf_far[k - FRONTMARGIN];
        in_buf_near[k] = tmp_buf_near[k - FRONTMARGIN];
    }
    for (k = (FRONTMARGIN + FRAMESIZE); k < FFTSIZE; k++)
    {
        in_buf_far[k] = 0.0;
        in_buf_near[k] = 0.0;
    }

    // Save current value 
    BufferCopy(in_buf_far, previn_buf_far, FFTSIZE);
    BufferCopy(in_buf_near, previn_buf_near, FFTSIZE);

    // Apply Window 
    apply_window(in_buf_far, Window, FFTSIZE);
    apply_window(in_buf_near, Window, FFTSIZE);

    // Copy to FFT Buffer 
    BufferCopy(in_buf_far, fftbuf_far, FFTSIZE);
    BufferCopy(in_buf_near, fftbuf_near, FFTSIZE);

    //  FFT
    r_fft(fftbuf_far, 1);
    r_fft(fftbuf_near, 1);

    for (i = 0; i < FFTSIZE; i++)
    {
        farend[i] = fftbuf_far[i];
        nearend[i] = fftbuf_near[i];
    }

    //------------------------fft---------------------//

    for (i = 0; i < H_FFTSIZE; i++)
    {
        // far-end conjugate
        conju_far[2 *i] = farend[2 *i];
        conju_far[2 *i + 1] =  - farend[2 *i + 1];

        // far-end and near-end correlation
        far_near[2 *i] = conju_far[2 *i] *nearend[2 *i];
        far_near[2 *i + 1] = conju_far[2 *i] *nearend[2 *i + 1];

        far_near[2 *i] += ( - conju_far[2 *i + 1] *nearend[2 *i + 1]);
        far_near[2 *i + 1] += conju_far[2 *i + 1] *nearend[2 *i];

        //far-end conjugate and far-end correlation
        far_far[2 *i] = farend[2 *i] *conju_far[2 *i];
        far_far[2 *i + 1] = farend[2 *i] *conju_far[2 *i + 1];

        far_far[2 *i] +=  - (farend[2 *i + 1] *conju_far[2 *i + 1]);
        far_far[2 *i + 1] += farend[2 *i + 1] *conju_far[2 *i];
    }

    for (i = LO_CHAN; i <= HI_CHAN; i++)
    {
        j1 = ch_table[i][0];
        j2 = ch_table[i][1];

        //--------------channel band-------------//

        for (j = j1; j <= j2; j++)
            far_power[i] += (SQ(farend[2 *j]) + SQ(farend[2 *j + 1]));
        far_power[i] /= (float)(j2 - j1 + 1);

        for (j = j1; j <= j2; j++)
            near_power[i] += (SQ(nearend[2 *j]) + SQ(nearend[2 *j + 1]));
        near_power[i] /= (float)(j2 - j1 + 1);

        for (j = j1; j <= j2; j++)
            far_near_power[i] += (SQ(far_near[2 *j]) + SQ(far_near[2 *j + 1]));
        far_near_power[i] /= (float)(j2 - j1 + 1);

        for (j = j1; j <= j2; j++)
            far_far_power[i] += (SQ(far_far[2 *j]) + SQ(far_far[2 *j + 1]));
        far_far_power[i] /= (float)(j2 - j1 + 1);

        long_far[i] = far_power[i];
        long_near[i] = near_power[i];
        long_far_near[i] = far_near_power[i];
        long_far_far[i] = far_far_power[i];

        //--------------magnitude estimation-------------//

        far_mag[i] = SQRT(long_far[i]);
        near_mag[i] = SQRT(long_near[i]);
        far_near_mag[i] = SQRT(long_far_near[i]);
        far_far_mag[i] = SQRT(long_far_far[i]);
    }

    para_Gv = 0.998f;
    dd = 0.92f;
    a_p = 0.3f;

    for (i = LO_CHAN; i <= HI_CHAN; i++)
    {
        j1 = ch_table[i][0];
        j2 = ch_table[i][1];

        if (far_near_mag[i] <= 10.0)
            far_near_mag[i] = 10.f;

        if (far_far_mag[i] <= 10.0)
            far_far_mag[i] = 10.f;

        //--------------echo path response estimation-------------//
        if (FrameNo == 0)
            a12[i] = far_near_mag[i];
        else
            a12[i] = para_Gv * a12[i] + (1.f - para_Gv) *far_near_mag[i];

        if (FrameNo == 0)
            a22[i] = far_far_mag[i];
        else
            a22[i] = para_Gv * a22[i] + (1.f - para_Gv) *far_far_mag[i];

        if (FrameNo == 0)
            temp_far_gain[i] = far_gain[i];

        //--------------eatimation gain of echo path response -------------//
        far_gain[i] = (float)(a12[i] / (a22[i] + epsi));

        if (far_gain[i] >= 1.0)
            far_gain[i] = 1.f;

        //--------------estimated echo path response -------------//	
        estimated_echo[i] = temp_far_gain[i] *far_mag[i];

        //------------------------------combined power of estimated echo and noise----------------------//

        temp_estimated_echo[i] = 1.2f * SQ(estimated_echo[i]) + evrc_noise[i];

        F_es[i] = SQ(temp_estimated_echo[i]);

        if (temp_estimated_echo[i] > long_near[i])
            temp_estimated_echo[i] = long_near[i];

        //------------------------------posteriori snr for integrated system ----------------------//
        post_snr[i] = (float)(long_near[i] / (temp_estimated_echo[i] + epsi));

        gammaf[i] = (float)((long_near[i] / (temp_estimated_echo[i] + epsi)) - 1.f);

        if (gammaf[i] >= 0.0)
            gammaf[i] = gammaf[i];
        else
            gammaf[i] = 0.f;

        if (FrameNo == 0)
            dd_snr[i] = (float)(long_near[i] / (temp_estimated_echo[i] + epsi));
        else
            dd_snr[i] = dd * snr_pri[i] + (1.f - dd) *gammaf[i];

        //---------------  malah scheme---------------//
        if (post_snr[i] > 3.0)
            m[i] = 0.f;
        else
            m[i] = 1.f;

        if (FrameNo == 0)
            a_opt[i] = 0.f;

        a_opt[i] = a_p * a_opt[i] + (1.f - a_p) *m[i];

        a_opt[i] = MAX1(a_opt[i], 0.01f);
        a_opt[i] = MIN1(a_opt[i], 0.99f);

        if (FrameNo == 0)
        {
            se_enhance_power[i] = long_near[i];
            prev_F_es[i] = SQ(temp_estimated_echo[i]);
        }

        //---------------  dicision-directed scheme---------------//
        pri_snr[i] = (float)(dd *(se_enhance_power[i] / (prev_F_es[i] + epsi)) + (1.f - dd) *post_snr[i]);

        //---------------  echo suppression gain based on wiener filter---------------//		
        wie_gain[i] = pri_snr[i] / (1.f + pri_snr[i]);

        //-----------------residual echo suppression based on soft decison----------------//

        ftmp = (float)log(1.f + dd_snr[i]);
        llrt[i] = (post_snr[i]) *dd_snr[i] / (1.f + dd_snr[i]);
        llrt[i] -= ftmp;
        lrt[i] = SAFE_EXP(llrt[i]);

        lsap[i] = (float)(1.f / (1.f + ((1.f - a_opt[i]) / (a_opt[i] + epsi) *lrt[i])));
        lsap[i] = SAFE_SAP(lsap[i]);
    }

    for (i = LO_CHAN; i <= HI_CHAN; i++)
    {
        j1 = ch_table[i][0];
        j2 = ch_table[i][1];

        for (j = j1; j <= j2; j++)
        {
            nearend[2 *j] *= (wie_gain[i]*(1.f - lsap[i]));
            nearend[2 *j + 1] *= (wie_gain[i]*(1.f - lsap[i]));
        }

        se_enhance_power[i] = 0.f;

        for (j = j1; j <= j2; j++)
            se_enhance_power[i] += (SQ(nearend[2 *j]) + SQ(nearend[2 *j + 1]));
        se_enhance_power[i] /= (float)(j2 - j1 + 1);

        // enhanced signal //
        snr_pri[i] = (float)(se_enhance_power[i] / (temp_estimated_echo[i] + epsi));

        prev_F_es[i] = F_es[i];
    }

    //---------------------------------dtd-------------------------------
    sum_cross_zd = 0.0;
    sum_cross_ze = 0.0;

    for (i = LO_CHAN; i <= HI_CHAN; i++)
    {
        if (estimated_echo[i] == 0.0)
            estimated_echo[i] = 0.01f;

        if (near_mag[i] == 0.0)
            near_mag[i] = 0.01f;

        if (se_enhance_power[i] == 0.0)
            se_enhance_power[i] = 0.01f;

        dtd_para1 = 0.9f;

        if (FrameNo == 0)
        {
            cross_p_d[i] = SQ(estimated_echo[i]);
            cross_p_z[i] = SQ(near_mag[i]);
            cross_p_zd[i] = estimated_echo[i] *near_mag[i];
            cross_p_e[i] = se_enhance_power[i];
            cross_p_ze[i] = near_mag[i] *SQRT(se_enhance_power[i]);
        }
        else
        {
            cross_p_d[i] = dtd_para1 * cross_p_d[i] + (1.f - dtd_para1) *SQ(estimated_echo[i]);
            cross_p_z[i] = dtd_para1 * cross_p_d[i] + (1.f - dtd_para1) *SQ(near_mag[i]);
            cross_p_zd[i] = dtd_para1 * cross_p_d[i] + (1.f - dtd_para1) *estimated_echo[i] *near_mag[i];
            cross_p_e[i] = dtd_para1 * cross_p_e[i] + (1.f - dtd_para1) *se_enhance_power[i];
            cross_p_ze[i] = dtd_para1 * cross_p_ze[i] + (1.f - dtd_para1) *near_mag[i] *SQRT(se_enhance_power[i]);
        }

        cross_rho_zd[i] = (float)fabs(cross_p_zd[i] / (SQRT(cross_p_d[i] *cross_p_z[i])));
        cross_rho_ze[i] = (float)fabs(cross_p_ze[i] / (SQRT(cross_p_e[i] *cross_p_z[i])));

        if (cross_rho_zd[i] > 1.0)
            cross_rho_zd[i] = 1.0;

        if (cross_rho_ze[i] > 1.0)
            cross_rho_ze[i] = 1.0;

        sum_cross_zd += cross_rho_zd[i];
        sum_cross_ze += cross_rho_ze[i];
    }

    sum_cross_zd /= NUM_CHAN;
    sum_cross_ze /= NUM_CHAN;

    if (sum_cross_zd < 0.8 && sum_cross_ze > 0.64)
        dtd_flag = 1;
    else
        dtd_flag = 0;

    for (i = LO_CHAN; i <= HI_CHAN; i++)
    {
        if (dtd_flag == 0)
            temp_far_gain[i] = far_gain[i];
    }

    for (i = 0; i < FFTSIZE; i++)
        ifft_buf_near[i] = (float)nearend[i];

    //-----------IFFT-----------	

    r_fft(ifft_buf_near,  - 1);

    BufferCopy(ifft_buf_near, synth_buf_near, FFTSIZE);

    // Overlap-Add 
    for (k = 0; k < (FFTSIZE - FRAMESIZE); k++)
        out_buf_near[k] = synth_buf_near[k] + prevsynth_buf_near[k + FRAMESIZE];

    for (k = (FFTSIZE - FRAMESIZE); k < FRAMESIZE; k++)
        out_buf_near[k] = synth_buf_near[k];

    BufferCopy(synth_buf_near, prevsynth_buf_near, FFTSIZE);

    deemphasis(FRAMESIZE, out_buf_far, &deemph_last_AES);

    for (k = 0; k < FRAMESIZE; k++)
        out_buf[k] = (short)out_buf_near[k];

}

void preemphasis1(short len1, float *buf1, float *prev1)
{
    short k;
    float temp1;

    for (k = 0; k < len1; k++)
    {
        temp1 = buf1[k];
        buf1[k] = buf1[k] - (ALPHA **prev1);
        *prev1 = temp1;
    }
}

void modified_bessel(float w, float *a0, float *a1)
{
    short i;
    float fac1, fac2, power1, power2;

    *a0 = 1.f;
    *a1 = w * 0.5f;
    fac1 = 1.f;
    fac2 = 1.f;
    power1 =  *a0;
    power2 =  *a1;
    for (i = 1; i < NMAX; i++)
    {
        fac1 *= ((float)(i));
        fac2 *= ((float)(i + 1));
        power1 *= ((0.5f *w)*(0.5f *w));
        power2 *= ((0.5f *w)*(0.5f *w));
        *a0 += (power1 / (fac1 *fac1));
        *a1 += (power2 / (fac1 *fac2));
    }
}

float compute_gain(float pri, float post)
{
    float gamma, eta, v, f0, f1, ga;

    gamma = post;
    eta = pri;

    v = (float)((pri / (pri + 1.0)) *gamma);
    modified_bessel(v *0.5f, &f0, &f1);
    ga = ((1.f + v) *f0) + (v *f1);

    ga *= (float)(sqrt(v) / gamma);
    ga *= SAFE_EXP( - 0.5f * v);
    ga *= (float)(sqrt(PI1) *0.5);

    if (ga < MINGAIN)
        ga = MINGAIN;
    else if (ga > 1.0)
        ga = 1.f;

    return (ga);
}

float SAFE_EXP(float x)
{

    if (x < (0.0 - 10.0))
        return ((float)exp( - 10.0));
    else if (x > 10.0)
        return ((float)exp(10.0));
    else
        return ((float)exp(x));
}

float SAFE_SAP(float x)
{

    if (x > 1.0)
        return (1.0);
    else if (x < 0.0)
        return (0.0);
    else
        return (x);
}
