#ifdef FE_TEST
#include "fe_init.h"
#include "policy.h"
#include "enh_formant.h"
#include "equalizer.h"
#include "basic_op.h"
#include "rom_s-peaker.h"
#include "Common.h"
#include "AHDR.h"

volatile uint16_t g_ui16_frame_no = 0;

static void init()
{
  Init_fe();
  init_vad();
  reset_AHDR();
}

void formant_enhancement( void )
{
  int16_t i16_buf16[FRAMESIZE];
  
  g_ui16_frame_no++;
  
  if( g_ui32_acnt == 0 )
  {
    init();
  }

  for (int j = 0; j < FRAMESIZE; j++)
  {
    i16_buf16[j] = g_ui16_inputdata[(g_ui32_acnt++)*2];
  }
  
  if( g_ui16_fe_enable )
  {
    FormantEnhancement(i16_buf16, 2);
  
    if (AcousticHighDynamicRange_exe(i16_buf16, FRAMESIZE) != NO_ERROR)
    {
      for (int j = 0; j < FRAMESIZE; j++)
      {
        if( g_ui16_frame_no % 2 )
        {
          g_ui16_data2[j*2] = (uint16_t)(0);
          g_ui16_data2[j*2+1] = (uint16_t)(0);
        }
        else
        {
          g_ui16_data[j*2] = (uint16_t)(0);
          g_ui16_data[j*2+1] = (uint16_t)(0);
        }
        return;
      }
    }
  }

  for (int j = 0; j < FRAMESIZE; j++)
  {
    if( g_ui16_frame_no % 2 )
    {
      g_ui16_data[j*2] = (uint16_t)i16_buf16[j];
      g_ui16_data[j*2+1] = (uint16_t)(0);
    }
    else
    {
      g_ui16_data2[j*2] = (uint16_t)i16_buf16[j];
      g_ui16_data2[j*2+1] = (uint16_t)(0);
    }
  }

  if( g_ui32_acnt == 50000 )
  {
    g_ui32_acnt = 0;
    g_ui16_frame_no = 0;
  }

}
#endif