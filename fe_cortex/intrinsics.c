



int _iabs(int val)
{
    if ((val) < 0)
    {
        return  - val;
    }
    else
    {
        return val;
    }
}

int _iround(int Val, int Bit)
{
    return ((Val + (1 << (Bit - 1))) >> (Bit));
}
